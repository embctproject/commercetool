package com.springboot.commercetools.controller;

import javax.money.CurrencyContextBuilder;
import javax.money.CurrencyUnit;
import javax.servlet.http.HttpServletRequest;

import org.javamoney.moneta.CurrencyUnitBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.springboot.commercetools.data.User;

import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.carts.CartDraft;
import io.sphere.sdk.carts.CartDraftBuilder;
import io.sphere.sdk.carts.commands.CartCreateCommand;
import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.client.ErrorResponseException;
import io.sphere.sdk.customers.Customer;
import io.sphere.sdk.customers.CustomerSignInResult;
import io.sphere.sdk.customers.commands.CustomerSignInCommand;

@Controller
@SessionAttributes("sessionUser")
public class LoginPageController
    {

	@Autowired
	private BlockingSphereClient sphereClient;

	@GetMapping("/login")
	public String getLogin(Model model,HttpServletRequest httpServletRequest)
	    {
		Customer sessionUser = (Customer) httpServletRequest.getSession().getAttribute("sessionUser");
		if(null==sessionUser)
		    {
			model.addAttribute("user", new User());
			return "loginpage";
		    }
		else
		    {
			return "redirect:/products?page=0&limit=20";
		    }
	    }

	@PostMapping(value = "/login")
	public String login(final Model model, @ModelAttribute("user") User user, HttpServletRequest httpServletRequest,
		RedirectAttributes redir)
	    {

		final CustomerSignInCommand signInCommand = CustomerSignInCommand.of(user.getEmail(),
			user.getPassword());
		Cart cart = null;
		try
		    {
			final CustomerSignInResult signInResult = sphereClient.executeBlocking(signInCommand);
			model.addAttribute("customerName", signInResult.getCustomer().getName());

			if (signInResult.getCart() == null)
			    {
				cart = createCart(signInResult.getCustomer());
			    } else
			    {
				cart = signInResult.getCart();
			    }
			httpServletRequest.getSession().setAttribute("sessionCart", cart);
			httpServletRequest.getSession().getId();
			httpServletRequest.getSession().setAttribute("sessionUser", signInResult.getCustomer());
		    } catch (ErrorResponseException e)
		    {
			model.addAttribute("errorMessage", e.getErrors().get(0).getMessage());
			return getLogin(model,httpServletRequest);
		    }

		httpServletRequest.getSession().setAttribute("cartCount", cart.getLineItems().size());
		return "redirect:/products?page=0&limit=20";

	    }

	private Cart createCart(Customer customer)
	    {
		CurrencyUnit currencyUnit = CurrencyUnitBuilder.of("EUR", CurrencyContextBuilder.of("default").build())
			.build();
		CartDraftBuilder cartDraftBuilder = CartDraftBuilder.of(currencyUnit).customerId(customer.getId());
		CartDraft cartDraft = cartDraftBuilder.build();
		Cart cart = sphereClient.executeBlocking(CartCreateCommand.of(cartDraft));
		return cart;
	    }
    }
