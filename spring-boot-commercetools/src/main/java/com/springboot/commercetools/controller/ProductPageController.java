package com.springboot.commercetools.controller;

import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.products.ProductProjection;
import io.sphere.sdk.products.queries.ProductProjectionQuery;
import io.sphere.sdk.queries.PagedQueryResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

@Controller
public class ProductPageController {

	@Autowired
	private BlockingSphereClient sphereClient;

	private static final String PROPERTIES_FILE_NAME = "application.properties";
	Properties properties = null;

	public ProductPageController() {
		try {
			properties = PropertiesLoaderUtils.loadAllProperties(PROPERTIES_FILE_NAME);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public CompletionStage<String> getProductList(final Model model,
			@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "40") int limit) {
		int offset = (page * limit) + 1;
		return sphereClient
				.execute(ProductProjectionQuery.ofCurrent().withOffset(offset).withLimit(limit)
						.plusSort(p -> p.createdAt().sort().asc()))
				.thenApply((PagedQueryResult<ProductProjection> queryResult) -> {
					model.addAttribute("allProducts", queryResult.getResults());
					model.addAttribute("locale", Locale.ENGLISH);
					model.addAttribute("totalPages", (int) Math.ceil(queryResult.getTotal() / limit));
					return "productlistingpage";
				});
	}

}