package com.springboot.commercetools.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletionStage;

import javax.money.CurrencyContextBuilder;
import javax.money.CurrencyUnit;

import org.javamoney.moneta.CurrencyUnitBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.neovisionaries.i18n.CountryCode;
import com.springboot.commercetools.data.CustomerAddress;
import com.springboot.commercetools.data.UserDetail;

import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.carts.CartDraft;
import io.sphere.sdk.carts.CartDraftBuilder;
import io.sphere.sdk.carts.commands.CartCreateCommand;
import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.customergroups.CustomerGroup;
import io.sphere.sdk.customergroups.queries.CustomerGroupByIdGet;
import io.sphere.sdk.customers.Customer;
import io.sphere.sdk.customers.CustomerDraft;
import io.sphere.sdk.customers.CustomerDraftBuilder;
import io.sphere.sdk.customers.CustomerName;
import io.sphere.sdk.customers.CustomerSignInResult;
import io.sphere.sdk.customers.commands.CustomerCreateCommand;
import io.sphere.sdk.models.Address;

@Controller
public class RegistrationPageController
    {

	@Autowired
	private BlockingSphereClient sphereClient;

	@GetMapping("/registration")
	public String getRegistration(Model model)
	    {
		model.addAttribute("userForm", new UserDetail());
		model.addAttribute("titleData", referenceData());
		return "registrationpage";
	    }

	@PostMapping(value = "/registration")
	public CompletionStage<String> registration(final Model model, @ModelAttribute("userForm") UserDetail userForm)
	    {
		LocalDate firstDay_2014 = LocalDate.of(userForm.getDateOfBirth().getYear() + 1900,
			userForm.getDateOfBirth().getMonth() + 1, userForm.getDateOfBirth().getDay());
		CustomerDraftBuilder customerDraftBuilder = CustomerDraftBuilder
			.of(CustomerName.of(userForm.getTitle(), userForm.getFirstName(), userForm.getMiddleName(),
				userForm.getLastName()), userForm.getEmail(), userForm.getPassword())
			.companyName(userForm.getCompanyName()).dateOfBirth(firstDay_2014)
			// customerDraftBuilder.anonymousCartId("11111");
			.emailVerified(userForm.getEmailVerified()).vatId(userForm.getVatId())
			.externalId(userForm.getExternalId()).customerNumber(userForm.getCustomerNumber());
		List<Address> addresses = getAddressList(userForm.getShippingAddress());
		if (null != userForm.getBillingAddress())
		    {
			addresses.addAll(getAddressList(userForm.getBillingAddress()));
		    }
		CustomerDraft customerDraft = customerDraftBuilder.build().withAddresses(addresses)
			.withDefaultShippingAddress(0);
		return sphereClient.execute(CustomerCreateCommand.of(customerDraft))
			.thenApply((CustomerSignInResult queryResult) ->
			    {
				createCart(queryResult.getCustomer());
				model.addAttribute("customerName", queryResult.getCustomer().getFirstName());
				model.addAttribute("titleData", referenceData());
				return "registrationpage";
			    });
	    }

	private List<Address> getAddressList(CustomerAddress customerAddress)
	    {
		List<Address> addressList = new LinkedList<Address>();
		Address address = Address.of(CountryCode.valueOf(customerAddress.getCountry()))
			.withStreetName(customerAddress.getStreetName())
			.withStreetNumber(customerAddress.getStreetNumber()).withCity(customerAddress.getCity())
			.withPostalCode(customerAddress.getPostalCode()).withMobile(customerAddress.getMobile());
		addressList.add(address);
		return addressList;
	    }

	private void createCart(Customer customer)
	    {
		CurrencyUnit currencyUnit = CurrencyUnitBuilder.of("EUR", CurrencyContextBuilder.of("default").build())
			.build();
		CartDraftBuilder cartDraftBuilder = CartDraftBuilder.of(currencyUnit).customerId(customer.getId());
		CartDraft cartDraft = cartDraftBuilder.build()
			.withShippingAddress(customer.getShippingAddresses().get(0));
		Cart cart = sphereClient.executeBlocking(CartCreateCommand.of(cartDraft));
		System.out.println("cart ID : " + cart.getId());
		System.out.println("customer id : " + cart.getCustomerId());
	    }

	protected List<String> referenceData()
	    {
		List<String> titleList = new ArrayList<String>();
		titleList.add("Mr.");
		titleList.add("Mrs.");
		return titleList;
	    }

    }