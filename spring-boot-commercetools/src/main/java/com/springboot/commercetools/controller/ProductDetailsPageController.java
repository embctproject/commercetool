package com.springboot.commercetools.controller;

import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.customergroups.CustomerGroup;
import io.sphere.sdk.customergroups.queries.CustomerGroupQuery;

import io.sphere.sdk.products.ProductProjection;
import io.sphere.sdk.products.ProductVariant;
import io.sphere.sdk.products.queries.ProductProjectionQuery;
import io.sphere.sdk.queries.PagedQueryResult;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.concurrent.CompletionStage;
import java.util.List;
import java.util.Locale;

@Controller
public class ProductDetailsPageController
    {

	@Autowired
	private BlockingSphereClient sphereClient;

	@RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
	public CompletionStage<String> getProductsById(final Model model, @PathVariable("id") String id)
	    {
		return sphereClient.execute(ProductProjectionQuery.ofCurrent().plusPredicates(m -> m.id().is(id)))
			.thenApply((PagedQueryResult<ProductProjection> queryResult) ->
			    {
				model.addAttribute("allProducts", queryResult.getResults());
				model.addAttribute("locale", Locale.ENGLISH);
				// getAvailability(queryResult.getResults());
				return "productdetailspage";
			    });
	    }

	public void getAvailability(List<ProductProjection> product)
	    {
		for (ProductProjection productProjection : product)
		    {
			List<ProductVariant> variants = productProjection.getVariants();
			for (ProductVariant productVariant : variants)
			    {
				System.out.println("SKU " + productVariant.getSku());
				if (productVariant.getAvailability() != null)
				    {
					System.out.println(
						"Availability : " + productVariant.getAvailability().isOnStock());
				    }
			    }
		    }
	    }

	@RequestMapping(value = "/customer-groups/key={key}", method = RequestMethod.GET)
	public CompletionStage<String> getCustomerGroup(final Model model, @PathVariable("key") String key)
	    {
		return sphereClient.execute(CustomerGroupQuery.of().plusPredicates(m -> m.name().is(key)))
			.thenApply((PagedQueryResult<CustomerGroup> queryResult) ->
			    {
				model.addAttribute("key", queryResult.getResults().get(0).getName());
				return "product/customer";
			    });
	    }

    }