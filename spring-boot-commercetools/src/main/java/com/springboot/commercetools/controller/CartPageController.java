package com.springboot.commercetools.controller;

import java.util.List;
import java.util.Locale;

import javax.money.MonetaryAmount;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.springboot.commercetools.service.CartService;

import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.carts.LineItem;
import io.sphere.sdk.carts.commands.CartUpdateCommand;
import io.sphere.sdk.carts.commands.updateactions.AddLineItem;
import io.sphere.sdk.carts.commands.updateactions.ChangeLineItemQuantity;
import io.sphere.sdk.carts.queries.CartByCustomerIdGet;
import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.client.ErrorResponseException;
import io.sphere.sdk.customers.Customer;

@Controller
public class CartPageController
    {
	@Autowired
	private BlockingSphereClient sphereClient;

	@Autowired
	private CartService cartService;

	@GetMapping("/showcart")
	public String getCart(Model model, HttpServletRequest httpServletRequest)
	    {
		Customer sessionUser = (Customer) httpServletRequest.getSession().getAttribute("sessionUser");
		Cart cart = sphereClient.executeBlocking(CartByCustomerIdGet.of(sessionUser.getId()));
		model.addAttribute("cart", cart);
		model.addAttribute("locale", Locale.ENGLISH);
		MonetaryAmount monetaryAmount = cartService.calculateSubTotal(cart);
		model.addAttribute("totalPrice", monetaryAmount);
		return "cartpage";
	    }

	@PostMapping("/carts")
	public String addToCart(Model model, HttpServletRequest httpServletRequest,
		@RequestParam("productId") String productId,
		@RequestParam(value = "variantId", defaultValue = "0") int variantId,
		@RequestParam("quantity") Long quantity, RedirectAttributes redir)
	    {
		httpServletRequest.getSession().getId();
		Customer sessionUser = (Customer) httpServletRequest.getSession().getAttribute("sessionUser");
		Cart cart = null;
		if (sessionUser == null)
		    {
			cart = cartService.createCart(sessionUser);
		    } else
		    {
			cart = sphereClient.executeBlocking(CartByCustomerIdGet.of(sessionUser.getId()));
		    }
		try
		    {
			sphereClient.executeBlocking(
				CartUpdateCommand.of(cart, AddLineItem.of(productId, variantId, quantity)));
		    } catch (ErrorResponseException e)
		    {
			redir.addFlashAttribute("errorMessage", e.getErrors().get(0).getMessage());
			return "redirect:products/" + productId;
		    }
		model.addAttribute("cart", cart);
		model.addAttribute("locale", Locale.ENGLISH);
		MonetaryAmount monetaryAmount = cartService.calculateSubTotal(cart);
		model.addAttribute("totalPrice", monetaryAmount);
		return "cartpage";
	    }

	@PostMapping("/updatecarts")
	public String updateCart(Model model, HttpServletRequest httpServletRequest,
		@RequestParam("productId") String productId,
		@RequestParam(value = "variantId", defaultValue = "0") int variantId,
		@RequestParam("quantity") Long quantity)
	    {
		System.out.println("productId : " + productId);
		System.out.println("variantId : " + variantId);
		System.out.println("quantity : " + quantity);
		Customer sessionUser = (Customer) httpServletRequest.getSession().getAttribute("sessionUser");

		Cart cart = sphereClient.executeBlocking(CartByCustomerIdGet.of(sessionUser.getId()));
		List<LineItem> lineItem = cart.getLineItems();
		for (LineItem lineItem2 : lineItem)
		    {
			if (lineItem2.getProductId().equals(productId)
				&& lineItem2.getVariant().getId().equals(variantId))
			    {
				sphereClient.executeBlocking(
					CartUpdateCommand.of(cart, ChangeLineItemQuantity.of(lineItem2, quantity)));
			    }
		    }
		String redirectURL = "/showcart";
		return "redirect:" + redirectURL;
	    }

    }
