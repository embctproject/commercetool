package com.springboot.commercetools.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.money.MonetaryAmount;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

import com.neovisionaries.i18n.CountryCode;
import com.springboot.commercetools.data.CustomerAddress;
import com.springboot.commercetools.service.CartService;

import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.carts.commands.CartUpdateCommand;
import io.sphere.sdk.carts.commands.updateactions.SetBillingAddress;
import io.sphere.sdk.carts.commands.updateactions.SetCustomShippingMethod;
import io.sphere.sdk.carts.commands.updateactions.SetCustomerEmail;
import io.sphere.sdk.carts.commands.updateactions.SetShippingAddress;
import io.sphere.sdk.carts.queries.CartByCustomerIdGet;
import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.customers.Customer;
import io.sphere.sdk.models.Address;
import io.sphere.sdk.orders.Order;
import io.sphere.sdk.orders.OrderFromCartDraft;
import io.sphere.sdk.orders.PaymentState;
import io.sphere.sdk.orders.ShipmentState;
import io.sphere.sdk.orders.commands.OrderFromCartCreateCommand;
import io.sphere.sdk.orders.commands.OrderUpdateCommand;
import io.sphere.sdk.orders.commands.updateactions.ChangePaymentState;
import io.sphere.sdk.orders.commands.updateactions.ChangeShipmentState;
import io.sphere.sdk.shippingmethods.ShippingRate;
import io.sphere.sdk.taxcategories.TaxCategory;
import io.sphere.sdk.taxcategories.TaxCategoryDraft;
import io.sphere.sdk.taxcategories.TaxRate;
import io.sphere.sdk.taxcategories.commands.TaxCategoryCreateCommand;
import io.sphere.sdk.taxcategories.queries.TaxCategoryQuery;
import io.sphere.sdk.utils.MoneyImpl;

@Controller
public class CheckoutPageController {

	@Autowired
	private BlockingSphereClient sphereClient;

	@Autowired
	private CartService cartService;

	@PostMapping(value = "/checkout")
	public String placeOrder(final Model model, final HttpServletRequest httpServletRequest) {
		Customer sessionUser = (Customer) httpServletRequest.getSession().getAttribute("sessionUser");
		// Order order=createOrderFromCart(sessionUser);
		Address billingAddress = sessionUser.getDefaultBillingAddress();
		Address shippingAddress = sessionUser.getDefaultShippingAddress();
		model.addAttribute("shippingAddress", shippingAddress);
		model.addAttribute("newShippingAddress", new CustomerAddress());
		model.addAttribute("billingAddress", billingAddress);
		Cart cart = sphereClient.executeBlocking(CartByCustomerIdGet.of(sessionUser.getId()));
		model.addAttribute("cart", cart);
		model.addAttribute("locale", Locale.ENGLISH);
		MonetaryAmount monetaryAmount = cartService.calculateSubTotal(cart);
		model.addAttribute("totalPrice", monetaryAmount);
		return "checkoutpage";
	}
}
