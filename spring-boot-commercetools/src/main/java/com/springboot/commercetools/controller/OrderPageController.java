package com.springboot.commercetools.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.money.MonetaryAmount;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

import com.neovisionaries.i18n.CountryCode;

import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.carts.commands.CartUpdateCommand;
import io.sphere.sdk.carts.commands.updateactions.SetBillingAddress;
import io.sphere.sdk.carts.commands.updateactions.SetCustomShippingMethod;
import io.sphere.sdk.carts.commands.updateactions.SetCustomerEmail;
import io.sphere.sdk.carts.commands.updateactions.SetShippingAddress;
import io.sphere.sdk.carts.queries.CartByCustomerIdGet;
import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.customers.Customer;
import io.sphere.sdk.orders.Order;
import io.sphere.sdk.orders.OrderFromCartDraft;
import io.sphere.sdk.orders.PaymentState;
import io.sphere.sdk.orders.ShipmentState;
import io.sphere.sdk.orders.commands.OrderFromCartCreateCommand;
import io.sphere.sdk.orders.commands.OrderUpdateCommand;
import io.sphere.sdk.orders.commands.updateactions.ChangePaymentState;
import io.sphere.sdk.orders.commands.updateactions.ChangeShipmentState;
import io.sphere.sdk.shippingmethods.ShippingRate;
import io.sphere.sdk.taxcategories.TaxCategory;
import io.sphere.sdk.taxcategories.TaxCategoryDraft;
import io.sphere.sdk.taxcategories.TaxCategoryDraftBuilder;
import io.sphere.sdk.taxcategories.TaxRate;
import io.sphere.sdk.taxcategories.TaxRateDraft;
import io.sphere.sdk.taxcategories.commands.TaxCategoryCreateCommand;
import io.sphere.sdk.taxcategories.queries.TaxCategoryQuery;
import io.sphere.sdk.utils.MoneyImpl;

@Controller
public class OrderPageController {

	@Autowired
	private BlockingSphereClient sphereClient;

	@PostMapping(value = "/orders")
	public String placeOrder(final Model model, final HttpServletRequest httpServletRequest) {
		Customer sessionUser = (Customer) httpServletRequest.getSession().getAttribute("sessionUser");
		Order order = createOrderFromCart(sessionUser);
		model.addAttribute("order", order);
		return "orderconfirmationpage";
	}

	private Order createOrderFromCart(Customer customer) {
		Cart cart = sphereClient.executeBlocking(CartByCustomerIdGet.of(customer.getId()));
		final TaxCategory taxCategory = defaultTaxCategory(customer);
		final MonetaryAmount amount = MoneyImpl.ofCents(10, "EUR");
		final SetCustomShippingMethod shippingMethodAction = SetCustomShippingMethod.of("custom shipping method",
				ShippingRate.of(amount), taxCategory);
		final SetCustomerEmail emailAction = SetCustomerEmail.of(customer.getEmail());
		Cart updatedCart = sphereClient.executeBlocking(CartUpdateCommand.of(cart,
				Arrays.asList(SetBillingAddress.of(customer.getDefaultBillingAddress()),
						SetShippingAddress.of(customer.getDefaultShippingAddress()), shippingMethodAction,
						emailAction)));
		OrderFromCartDraft orderFromCartDraft = OrderFromCartDraft.of(updatedCart,
				UUID.randomUUID().toString().substring(0, 5), PaymentState.PAID);
		final Order order = sphereClient.executeBlocking(OrderFromCartCreateCommand.of(orderFromCartDraft));
		sphereClient.executeBlocking(OrderUpdateCommand.of(order, Arrays
				.asList(ChangeShipmentState.of(ShipmentState.READY), ChangePaymentState.of(PaymentState.PENDING))));

		// final CustomerSignInCommand signInCommand =
		// CustomerSignInCommand.of(customer.getEmail(),
		// customer.getPassword(), cart.getId());
		// final CustomerSignInResult signInResult =
		// sphereClient.executeBlocking(signInCommand);
		return order;
	}

	public TaxCategory defaultTaxCategory(Customer customer) {
		final String name = "sdk-default-tax-cat-1";

		final TaxRateDraft taxRate = TaxRateDraft.of("xyz", 0.20, true, CountryCode.valueOf("DE"));
		List<TaxRateDraft> taxRateList = new ArrayList<TaxRateDraft>();
		taxRateList.add(taxRate);
		final TaxCategoryDraft categoryDraft = TaxCategoryDraft.of(name, taxRateList);
		return sphereClient.executeBlocking(TaxCategoryQuery.of().byName(name)).head()
				.orElseGet(() -> sphereClient.executeBlocking(TaxCategoryCreateCommand.of(categoryDraft)));
	}
}
