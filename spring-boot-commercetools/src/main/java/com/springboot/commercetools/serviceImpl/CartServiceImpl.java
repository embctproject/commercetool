package com.springboot.commercetools.serviceImpl;

import java.util.UUID;

import javax.money.CurrencyContextBuilder;
import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;

import org.javamoney.moneta.CurrencyUnitBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neovisionaries.i18n.CountryCode;
import com.springboot.commercetools.service.CartService;

import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.carts.CartDraft;
import io.sphere.sdk.carts.CartDraftBuilder;
import io.sphere.sdk.carts.CartLike;
import io.sphere.sdk.carts.commands.CartCreateCommand;
import io.sphere.sdk.client.BlockingSphereClient;
import io.sphere.sdk.customers.Customer;
import io.sphere.sdk.products.Price;
import io.sphere.sdk.utils.MoneyImpl;

@Service
public class CartServiceImpl implements CartService {

	@Autowired
	private BlockingSphereClient sphereClient;

	@Override
	public MonetaryAmount calculateSubTotal(final CartLike<?> cartLike) {
		final MonetaryAmount zeroAmount = MoneyImpl.ofCents(0, cartLike.getCurrency());
		return cartLike.getLineItems().stream().map(lineItem -> {
			final MonetaryAmount basePrice = calculateFinalPrice(lineItem.getPrice());
			return basePrice.multiply(lineItem.getQuantity());
		}).reduce(zeroAmount, (left, right) -> left.add(right));
	}

	@Override
	public Cart createCart(Customer customer) {
		CurrencyUnit currencyUnit = CurrencyUnitBuilder.of("EUR", CurrencyContextBuilder.of("default").build()).build();
		final String anonymousId = "UUI";
		final CartDraft cartDraft = CartDraft.of(currencyUnit).withCountry(CountryCode.valueOf("DE")).withAnonymousId(anonymousId);
		final Cart cart = sphereClient.executeBlocking(CartCreateCommand.of(cartDraft));
		return cart;
	}

	private MonetaryAmount calculateFinalPrice(final Price price) {
		final boolean hasProductDiscount = price.getDiscounted() != null;
		return hasProductDiscount ? price.getDiscounted().getValue() : price.getValue();
	}
}
