package com.springboot.commercetools.serviceImpl;

import javax.servlet.http.HttpServletRequest;

import com.springboot.commercetools.service.UserService;

import io.sphere.sdk.customers.Customer;

public class UserServiceImpl implements UserService {

	@Override
	public Customer getCurrentUser(HttpServletRequest httpServletRequest) {
		// TODO Auto-generated method stub
		Customer customer=(Customer) httpServletRequest.getSession().getAttribute("sessionUser");
		if(customer==null) {
			//customer=Anonymous
		}
		return customer;
	}

}
