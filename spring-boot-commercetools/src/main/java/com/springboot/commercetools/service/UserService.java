package com.springboot.commercetools.service;

import javax.servlet.http.HttpServletRequest;

import io.sphere.sdk.customers.Customer;

public interface UserService {
	public Customer getCurrentUser(HttpServletRequest httpServletRequest);
}
