package com.springboot.commercetools.service;

import javax.money.MonetaryAmount;

import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.carts.CartLike;
import io.sphere.sdk.customers.Customer;

public interface CartService {

	public MonetaryAmount calculateSubTotal(final CartLike<?> cartLike);
	
	public Cart createCart(Customer customer);
}
