<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Product Detail Page</title>
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${contextPath}/resources/css/style.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  </head>
  <body>
  	<jsp:include page="fragment/header.jsp" />
	<h1>Product Details</h1>
	<c:if test="${not empty errorMessage}">
		<div style="color:red;">${errorMessage}</div>
	</c:if>
	<c:forEach var="product" items="${allProducts}">
		<form action="/carts" method="post">
			<input type="hidden" value="${sessionUser.id}" name="customerId" /> 
			<input type="hidden" value="${product.id}" name="productId" /> 
			<select name="variantId">
				<c:forEach var="variant" items="${product.variants}">
					<option value="${variant.id}">${variant.sku}</option>
				</c:forEach>
			</select> <select name="quantity">
				<c:forEach var="i" begin="0" end="9">
					<option value="${i}">${i}</option>
				</c:forEach>
			</select>
			<div class="row">
				<div class="col-sm-6 col-md-4">
					<div class="thumbnail">
						<img width="200" height="200"
							src="${product.masterVariant.images[0].url}" />
					</div>
					<div class="caption">
						<h3>${product.name.find(locale).orElse('missing product name')}</h3>
						<h3>${product.id}</h3>
						<p>${product.description.find(locale).orElse('missing product description')}</p>

						<p>${product.masterVariant.prices[0].value.currency}</p>
						<p>${product.masterVariant.prices[0].value.number}</p>
					</div>
				</div>
			</div>
			<button type="submit" value="Add To Cart">Add To Cart</button>
		</form>
	</c:forEach>
	<jsp:include page="fragment/footer.jsp" />   	
</body>
</html>