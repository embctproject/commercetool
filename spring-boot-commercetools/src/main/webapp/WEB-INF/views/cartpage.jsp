<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
   <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cart Page</title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css" />
    <link rel="stylesheet" href="resources/css/style.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  </head>
  <body>
  	<jsp:include page="fragment/header.jsp" />

<div class="content">
    <div class="container-fluid">
        <h1 class="shopping-bag">Shopping Bag</h1>
        <div class="col-xs-12 nopadding cart-btn--container">
            <button type="button" title="Continue Shopping" class="cartbtns pull-left btn btn-info text-uppercase"><span>Continue Shopping</span></button>
            <button type="button" title="Proceed to Checkout" class="cartbtns pull-right btn btn-success text-uppercase"><span>Proceed to Checkout</span></button>
        </div>
        <table border="1" style="width:100%">
          		<tr>
          		  <th>Image</th>
          		  <th>Product Name</th>
          		  <th>Quantity</th>
          		  <th>Price</th>
          		  <th>Total Price</th>
          		</tr>
    		<c:forEach var="lineItem" items="${cart.lineItems}">
    		<form action="/updatecarts" method="post">
    			<input type="hidden" value="${cart.customerId}" name="customerId"/>
          		<input type="hidden" value="${lineItem.productId}" name="productId"/>
          		<input type="hidden" value="${lineItem.variant.id}" name="variantId"/>
          		
          		<tr>
          			<td><div class="thumbnail"> 
                    <img width="200" height="200" src="${lineItem.variant.images[0].url}" />
                </div></td>
          		<td> <h3>${lineItem.name.find(locale).orElse('missing product name')}</h3></td>
          		<td>
          			                        <select name="quantity">
          	<c:forEach var="i" begin="0" end="9" >
          		<c:choose>
          		<c:when test="${lineItem.quantity eq i}">
          			<option value="${i}" selected="selected">${i}</option>
          		</c:when>
          			<c:otherwise>
          				<option value="${i}">${i}</option>
          				
          			</c:otherwise>
          		</c:choose>
          	</c:forEach>
          </select>
          			<button type="submit" value="Update Cart">Update Cart</button>
          		</td>
          		<td>
          		<p> ${lineItem.price.value.currency}</p> 
                        <p> ${lineItem.price.value.number}</p> 
          		</td>
          		<td>${lineItem.totalPrice.currency}&nbsp;${lineItem.totalPrice.number}</td>
          		</tr>
          		
               
                  </form>  
    		</c:forEach>
    		 <tr>
    		 	<td colspan="5" align="right"><b>Total Price :</b> ${totalPrice.currency}&nbsp;${totalPrice.number}</td>
    		 </tr>
    		</table>
        <div class="order-summary pull-right col-xs-12 col-sm-4">
            <div class="order-summary--container pull-left">
                

                <div class="order-price pull-left col-xs-12 nopadding">
                    <div class="pull-left col-xs-12 nopadding summary-section">
                        <div class="pull-left text-left col-xs-6 nopadding">Summary</div>
                        <div class="pull-right text-right col-xs-6 nopadding">$200.00</div>
                    </div>
                    <div class="pull-left col-xs-12 nopadding summary-section">
                        <div class="pull-left text-left col-xs-6 nopadding">Tax</div>
                        <div class="pull-right text-right col-xs-6 nopadding">$50.00</div>
                    </div>

                    <div class="pull-left col-xs-12 nopadding summary-section">
                        <div class="pull-left text-left col-xs-6 nopadding">Shipping</div>
                        <div class="pull-right text-right col-xs-6 nopadding">$20.00</div>
                    </div>
                </div>

                <div class="summary-total pull-left col-xs-12 nopadding">
                    <div class="pull-left col-xs-12 nopadding summary-section">
                        <div class="pull-left text-left col-xs-6 nopadding">Total</div>
                        <div class="pull-right text-right col-xs-6 nopadding">$270.00</div>
                    </div>
                </div>
                <button type="button" title="Proceed to Checkout" class="cartbtns summary-checkout--btn btn btn-success text-uppercase"><span>Proceed to Checkout</span></button>
            </div>
        </div>
    </div>
</div>
    		<jsp:include page="fragment/footer.jsp" />  
  </body>
</html>
