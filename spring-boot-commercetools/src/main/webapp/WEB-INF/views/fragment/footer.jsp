<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<footer>
<div class="container-fluid">
        <div class="footer pull-left">
            <div class="pull-left col-xs-4">
                <h2>Shopping Categories</h2>
                <ul class="row">
                    <li><a href="javascript:;">Lorem Ipsum</a></li>
                    <li><a href="javascript:;">Lorem Ipsum</a></li>
                    <li><a href="javascript:;">Lorem Ipsum</a></li>
                    <li><a href="javascript:;">Lorem Ipsum</a></li>
                    <li><a href="javascript:;">Lorem Ipsum</a></li>
                    <li><a href="javascript:;">Lorem Ipsum</a></li>
                    <li><a href="javascript:;">Lorem Ipsum</a></li>
                    <li><a href="javascript:;">Lorem Ipsum</a></li>
                    <li><a href="javascript:;">Lorem Ipsum</a></li>
                </ul>
            </div>

            <div class="pull-left col-xs-4">
                <h2>Get to Know Us</h2>
                <ul class="row">
                    <li><a href="javascript:;">About Us</a></li>
                    <li><a href="javascript:;">About Our Products</a></li>
                    <li><a href="javascript:;">Contact Us</a></li>
                    <li><a href="javascript:;">Careers</a></li>
                    <li><a href="javascript:;">Testimonials</a></li>
                </ul>
            </div>

            <div class="pull-left col-xs-4">
                 <h2>Let us Help You</h2>
                <ul class="row">
                    <li><a href="javascript:;">FAQs</a></li>
                    <li><a href="javascript:;">Shipping & Delivery</a></li>
                    <li><a href="javascript:;">Return Policy</a></li>
                    <li><a href="javascript:;">Security Policy</a></li>
                    <li><a href="javascript:;">Privacy Policy</a></li>
                    <li><a href="javascript:;">Terms and Conditions</a></li>
                    <li><a href="javascript:;">Terms of Sale</a></li>
                    <li><a href="javascript:;">Loyalty Program</a></li>
                </ul>
            </div>
        </div>
        <p class="copyright">&copy; 2017 Embitel. All Rights Reserved.</p>
    </div>
</footer>