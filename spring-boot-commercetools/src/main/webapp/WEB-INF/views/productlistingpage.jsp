<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Product Listing Page</title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css" />
    <link rel="stylesheet" href="resources/css/style.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  </head>
  <body>
  	<jsp:include page="fragment/header.jsp" />
  <div class="content">
    <div class="container-fluid main-content">

        <div class="" style="float: left; width: 100%;">
        <div class="col-lg-3 col-md-3 pull-left plp-left">
            <div class="filter-container">
                <h2 class="filterby-text">Filter By:</h2>


                <div class="filters collapsed" data-toggle="collapse" data-target="#Brand">Brand</div>
                  <ul id="Brand" class="collapse filter-wrappers">
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                  </ul>

                <div class="filters collapsed" data-toggle="collapse" data-target="#Size">Size</div>
                  <ul id="Size" class="collapse">
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                  </ul>

                <div class="filters collapsed" data-toggle="collapse" data-target="#Price">Price</div>
                  <ul id="Price" class="collapse">
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                  </ul>


                <div class="filters collapsed" data-toggle="collapse" data-target="#Gender">Gender</div>
                  <ul id="Gender" class="collapse">
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                  </ul>

                <div class="filters collapsed" data-toggle="collapse" data-target="#Color">Color</div>
                  <ul id="Color" class="collapse">
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <label class="checkboxcontainer">Lorem Ipsum
                                <input type="checkbox" />
                                <span class="checkmark"></span>
                            </label>
                        </a>
                    </li>
                  </ul>


               
            </div>
        </div>
        

        <div class="col-lg-9 col-md-9 pull-left plp-right">
            <div class="pull-left sort-container">
                <div class="pull-left product-count">${fn:length(allProducts)} Products</div>
                <div class="pull-right">
                    <span class="sort-by">Sort By</span>

                    <div class="dropdown pull-right">
                      <button class="sort-products dropdown-toggle" type="button" data-toggle="dropdown">Name
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu ">
                        <li><a href="#">Popularity</a></li>
                        <li><a href="#">Price</a></li>
                        <li><a href="#">Newest</a></li>
                      </ul>
                    </div>

                </div>
            </div>
            <ul class="col-xs-12 product-container">
            	<c:forEach var="product" items="${allProducts}">
                <li class="col-xs-3 products">
                    <div class="product-item">
                        <div class="img-container">
                            <a href="/products/${product.id}"><img src="${product.masterVariant.images[0].url}" class="img-responsive"/></a>
                        </div>

                        <div class="product-title--container text-center">
                            <a href="/products/${product.id}">${product.name.find(locale).orElse('missing product name')}</a>
                        </div>

                        <div class="product-price--container text-center">
                           <a href="/products/${product.id}"><span class="special-price">${product.masterVariant.prices[0].value.currency}&nbsp;${product.masterVariant.prices[0].value.number}</span></a>
                        </div>
                    </div>
                </li>
                </c:forEach>
            </ul>
        </div>
       </div>
    </div>
</div>

<jsp:include page="fragment/footer.jsp" /> 
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
  	
</body>
</html>