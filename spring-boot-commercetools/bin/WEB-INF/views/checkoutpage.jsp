<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Checkout Page</title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css" />
    <link rel="stylesheet" href="resources/css/style.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  </head>
  <body>
  	<jsp:include page="fragment/header.jsp" />
	<table>
	<tr>
	<td>Shipping Address :<br>
	${shippingAddress.streetNumber},${shippingAddress.streetName}<br>
	${shippingAddress.city},${shippingAddress.postalCode}<br>
	${shippingAddress.mobile}</td>
  	<tr>
  	<td>Billing Address : <br>
  	${billingAddress.streetNumber},${billingAddress.streetName}<br>
  	${billingAddress.city},${billingAddress.postalCode}<br>
  	${billingAddress.mobile}</td>
  	</tr>
	</table>
	<table border="1" style="width:100%">
          		<tr>
          		  <th>Image</th>
          		  <th>Product Name</th>
          		  <th>Quantity</th>
          		  <th>Price</th>
          		  <th>Total Price</th>
          		</tr>
    		<c:forEach var="lineItem" items="${cart.lineItems}">
          		<tr>
          			<td><div class="thumbnail"> 
                    <img width="200" height="200" src="${lineItem.variant.images[0].url}" />
                </div></td>
          		<td> <h3>${lineItem.name.find(locale).orElse('missing product name')}</h3></td>
          		<td>${lineItem.quantity}</td>
          		<td>
          		<p> ${lineItem.price.value.currency}</p> 
                        <p> ${lineItem.price.value.number}</p> 
          		</td>
          		<td>${lineItem.totalPrice.currency}&nbsp;${lineItem.totalPrice.number}</td>
          		</tr>
    		</c:forEach>
    		 <tr>
    		 	<td colspan="5" align="right"><b>Total Price :</b> ${totalPrice.currency}&nbsp;${totalPrice.number}</td>
    		 </tr>
    		</table>
    		<form action="/orders" method="post">
    			<button type="submit" value="Place Order">Place Order</button>
    		</form>
    		<jsp:include page="fragment/footer.jsp" />   	
</body>
</html>