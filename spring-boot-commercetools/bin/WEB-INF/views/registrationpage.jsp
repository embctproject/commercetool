<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Registration Page</title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css" />
    <link rel="stylesheet" href="resources/css/style.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  </head>
  <body>
  	<jsp:include page="fragment/header.jsp" />
	<div class="content">
    <form:form method="POST" modelAttribute="userForm" class="form-signin">
    <div class="login-container">
        <div class="login-wrapper">
            <h3 class="text-uppercase">Signup</h3>
            <div class="field">
            <spring:bind path="title">
                    <form:select path="title">
    					<form:options items="${titleData}" />
					</form:select>
            </spring:bind>
            </div>
            <div class="field">
            	<spring:bind path="firstName">
                    <form:input type="text" path="firstName" class="input-field" placeholder="firstName"/>
            	</spring:bind>
            </div>
            <div class="field">
                <spring:bind path="middleName">
                    <form:input type="text" path="middleName" class="input-field" placeholder="middleName"/>
            	</spring:bind>
            </div>

            <div class="field">
                <spring:bind path="lastName">
                    <form:input type="text" path="lastName" class="input-field" placeholder="lastName"/>
            	</spring:bind>
            </div>
            <div class="field">
                <spring:bind path="email">
                    <form:input type="text" path="email" class="input-field" placeholder="email"/>
            	</spring:bind>
            </div>

            <div class="field">
                <spring:bind path="password">
                    <form:input type="password" path="password" class="input-field" placeholder="password"/>
            	</spring:bind>
            </div>

            <div class="field">
                <spring:bind path="dateOfBirth">
                    <form:input type="text" path="dateOfBirth" class="input-field" placeholder="dateOfBirth"/>
            	</spring:bind>
            </div>
            <div class="field">
            <label class="checkboxcontainer">Email Verified
                 <spring:bind path="emailVerified">
                    <form:checkbox path="emailVerified" class="input-field" placeholder="emailVerified"/>
            	</spring:bind>
                 <span class="checkmark"></span>
            </label> 
            </div>        
            <div class="field">
                <spring:bind path="vatId">
                    <form:input type="text" path="vatId" class="input-field" placeholder="vatId"/>
            	</spring:bind>
            </div>
            <div class="field">
                <spring:bind path="customerNumber">
                    <form:input type="text" path="customerNumber" class="input-field" placeholder="customerNumber"/>
            	</spring:bind>
            </div>
            <div class="field">
                <spring:bind path="externalId">
                    <form:input type="text" path="externalId" class="input-field" placeholder="externalId"/>
            	</spring:bind>
            </div>
			<div class="field">
                <spring:bind path="companyName">
                    <form:input type="text" path="companyName" class="input-field" placeholder="companyName"/>
            	</spring:bind>
            </div>
            <div class="field">Address Details :</div>
            <div class="field">
                <spring:bind path="shippingAddress">
                    <form:input type="text" path="shippingAddress.streetNumber" class="input-field" placeholder="streetNumber"/>
            	</spring:bind>
            </div>
            <div class="field">
                <spring:bind path="shippingAddress">
                    <form:input type="text" path="shippingAddress.streetName" class="input-field" placeholder="streetName"/>
            	</spring:bind>
            </div>
            <div class="field">
                <spring:bind path="shippingAddress">
                    <form:input type="text" path="shippingAddress.city" class="input-field" placeholder="city"/>
            	</spring:bind>
            </div>
             <div class="field">
                <spring:bind path="shippingAddress">
                    <form:input type="text" path="shippingAddress.postalCode" class="input-field" placeholder="postalCode"/>
            	</spring:bind>
            </div>
            <div class="field">
                <spring:bind path="shippingAddress">
                    <form:input type="text" path="shippingAddress.country" class="input-field" placeholder="country"/>
            	</spring:bind>
            </div>
            <div class="field">
                <spring:bind path="shippingAddress">
                    <form:input type="text" path="shippingAddress.mobile" class="input-field" placeholder="mobile"/>
            	</spring:bind>
            </div>
            <%-- <div class="field">Billing Address :</div>
            <div class="field">
                <spring:bind path="billingAddress">
                    <form:input type="text" path="billingAddress.streetNumber" class="input-field" placeholder="streetNumber"/>
            	</spring:bind>
            </div>
            <div class="field">
                <spring:bind path="billingAddress">
                    <form:input type="text" path="billingAddress.streetName" class="input-field" placeholder="streetName"/>
            	</spring:bind>
            </div>
            <div class="field">
                <spring:bind path="billingAddress">
                    <form:input type="text" path="billingAddress.city" class="input-field" placeholder="city"/>
            	</spring:bind>
            </div>
             <div class="field">
                <spring:bind path="billingAddress">
                    <form:input type="text" path="billingAddress.postalCode" class="input-field" placeholder="postalCode"/>
            	</spring:bind>
            </div>
            <div class="field">
                <spring:bind path="billingAddress">
                    <form:input type="text" path="billingAddress.country" class="input-field" placeholder="country"/>
            	</spring:bind>
            </div>
            <div class="field">
                <spring:bind path="billingAddress">
                    <form:input type="text" path="billingAddress.mobile" class="input-field" placeholder="mobile"/>
            	</spring:bind>
            </div> --%>
            <div class="field">
                <button type="submit" class="text-uppercase primary-btn">Signup</button>
            </div>
        </div>
    </div>
	</form:form>
</div>	
	
    	<c:if test="${not empty customerName }">
    		Hello ${titleData}.${customerName}
    	</c:if>
     <jsp:include page="fragment/footer.jsp" />   	
  </body>
</html>
