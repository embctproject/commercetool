<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<header>
<div class="container-fluid blue-header">
        <div class="pull-left blue-header--info text-uppercase">
            <div class="pull-left blue-header--txt">Free fast delivery</div>
            <div class="pull-left blue-header--txt">Best prices</div>
            <div class="pull-left blue-header--txt">Wides Choice</div>
        </div>
        <div class="pull-right blue-header--right">
            <a href="/registration" class="header-register--link">Register</a>
            <a href="/login">Login</a>            
        </div>
    </div>

    <div class="container-fluid header">
        <div class="pull-left logo"><img src="http://lorempixel.com/200/70/" alt="logo" title="logo"/></div>
        <div class="pull-left header-search--container">
            <div class="searh-box">
                <input type="text" class="search-input" placeholder="What can we help you find?"/>
                <button type="button" id="" title="Search" class="serach-button pull-right"><span class="icon-serach">Search</span></button>
            </div>
        </div>
        <div class="pull-left header-cart text-right">
           <i class="fas fa-shopping-cart cartbag"></i>
            <a href="/showcart"><span class="badge cartcount">${cartCount}</span></a>
        </div>
    </div>
</header>